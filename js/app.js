/*Теоретичні питання

1. Як можна створити рядок у JavaScript?
за допомогою let або const назви рядка та значеня після занка дорівнює в лапках.

2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
Між одинарними та подвійними немає ніякої ріниці. Зворотніми дозволяють використовувати інтерполяцію і створювати багаторядкові рядки

3. Як перевірити, чи два рядки рівні між собою?
За допомогою знаку рівності та console.log();
Наприклад, console.log(str === str2);

4. Що повертає Date.now()?
Повертає теперішній час в unix фориаті, це відлік час з моменту запуску першого компютера на базі unix подібної системи.

5. Чим відрізняється Date.now() від new Date()?
Обидва варіанти повертають теперішній час, одив в форматі unix, цей час зазвичай використовується для HTTP запитів через, те, що в різних куточках
світа різний час і необхідно розуміти в який саме час був зроблений запит, а інший повертає час по грінвичу.
*/

/*Практичні завдання

1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом

(читається однаково зліва направо і справа наліво), або false в іншому випадку.*/

const str = "aabbaa"

function isPalindrome(str) {
    return str.split('').reverse().join('') === str;
}

console.log(isPalindrome(str));



/*2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити,
максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false,
якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:

// Рядок коротше 20 символів

    funcName('checked string', 20); // true

// Довжина рядка дорівнює 18 символів

funcName('checked string', 10); // false*/
    const formName = "Denys"
    function stringLength(formName){
        return formName.length <= 20;
    }

console.log(stringLength(formName));

/*3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt.
Функція повина повертати значення повних років на дату виклику функцію.*/


function getAge() {
    const birthDate = prompt("Enter your birth date (yyyy-mm-dd)");

    const birth = new Date(birthDate);
    const today = new Date();

    let age = today.getFullYear() - birth.getFullYear();
    let monthDifference = today.getMonth() - birth.getMonth();
    let dayDifference = today.getDate() - birth.getDate();

    if (monthDifference < 0 || (monthDifference === 0 && dayDifference < 0)) {
        age--;
    }

    return age;
}

alert(getAge());
